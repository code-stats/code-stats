/**
 * Code to execute on index page.
 */

import { get_live_update_socket, wait_for_load } from "../common/utils.js";
import IndexPageUpdater from "./index_page_updater.js";

let updater = null;

async function index_page() {
  await wait_for_load();
  updater = new IndexPageUpdater(get_live_update_socket());
}

export default index_page;
