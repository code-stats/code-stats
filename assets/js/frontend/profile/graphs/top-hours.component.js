import { el } from "redom";
import { Chart } from "chart.js";
import { XP_FORMATTER } from "../../../common/xp_utils.js";
import { format_mins } from "../../../common/utils.js";

const AM_COLOR = "rgba(10, 0, 178, 0.5)";
const PM_COLOR = "rgba(255, 200, 64, 0.5)";
const HOURS = Array(24)
  .fill()
  .map((_, i) => i.toString().padStart(2, "0"));

class TopHoursComponent {
  constructor() {
    this.el = el("canvas");
  }

  onmount() {
    const canvas_ctx = this.el.getContext("2d");
    const chartBackgroundColours = Array(12)
      .fill(AM_COLOR)
      .concat(Array(12).fill(PM_COLOR));
    this.chart = new Chart(canvas_ctx, {
      data: {
        labels: [...Array(24).keys()].map((int) =>
          String(int).padStart(2, "0")
        ),
        datasets: [
          {
            data: [],
            backgroundColor: chartBackgroundColours,
            yAxisID: "y",
          },
          {
            data: [],
            backgroundColor: chartBackgroundColours,
            yAxisID: "y1",
          },
        ],
      },
      type: "bar",
      options: {
        plugins: {
          legend: {
            display: false,
          },
          tooltip: {
            enabled: true,
            mode: "index",
            callbacks: {
              title: (tooltip_item) => {
                if (Array.isArray(tooltip_item) && tooltip_item.length > 0) {
                  const label = parseInt(tooltip_item[0].label, 10);
                  return `${label}–${label + 1}`;
                } else {
                  return "";
                }
              },
              label: (tooltip_item) => {
                return tooltip_item.datasetIndex === 1
                  ? `${tooltip_item.formattedValue} XP`
                  : `${format_mins(tooltip_item.raw.y)} spent in flow`;
              },
            },
          },
        },
        indexAxis: "x",
        scales: {
          x: {
            stacked: true,
            position: "center",
          },
          y: {
            stacked: true,
            stack: "a",
            reverse: true,
            ticks: {
              callback: (val) => (val === 0 ? "0" : format_mins(val)),
            },
            title: {
              display: true,
              text: "Flow time",
            },
            min: 0,
          },
          y1: {
            stacked: true,
            stack: "a",
            reverse: false,
            ticks: {
              callback: (val) => XP_FORMATTER.format(val),
            },
            title: {
              display: true,
              text: "XP",
            },
            min: 0,
          },
        },
      },
    });
  }

  update({ x: top_xp_hours, f: top_flow_hours }) {
    top_xp_hours = top_xp_hours.map((item) => ({
      x: item.h,
      y: item.x,
    }));
    top_flow_hours = top_flow_hours.map((item) => ({
      x: item.h,
      y: item.m,
    }));

    top_xp_hours = this._addMissing(top_xp_hours);
    top_flow_hours = this._addMissing(top_flow_hours);

    this.chart.data.datasets[1].data = top_xp_hours;
    this.chart.data.datasets[0].data = top_flow_hours;
    this.chart.update("none");
  }

  // Add missing hours to dataset to ensure AM/PM colours and tooltips are correct
  _addMissing(dataset) {
    const included_hours = new Set();

    for (const { x } of dataset) {
      included_hours.add(x);
    }

    for (const hour of HOURS) {
      if (!included_hours.has(hour)) {
        dataset.push({
          x: hour,
          y: 0,
        });
      }
    }

    return dataset.sort(({ x: x1 }, { x: x2 }) => {
      if (x1 > x2) {
        return 1;
      } else if (x2 > x1) {
        return -1;
      } else {
        return 0;
      }
    });
  }
}

export { TopHoursComponent };
