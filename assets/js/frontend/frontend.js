import common_run from "./common.js";
import Router from "./router.js";

import {
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  Legend,
  LineController,
  LineElement,
  LinearScale,
  PointElement,
  TimeScale,
  Tooltip,
} from "chart.js";
import "../common/chartjs-luxon-adapter.js";

Chart.register(
  BarElement,
  BarController,
  LineElement,
  LineController,
  PointElement,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Tooltip
);

common_run();

const router = new Router();
router.execute();
