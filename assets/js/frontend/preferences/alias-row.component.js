import { el } from "redom";

import { graphButton } from "../../common/graph-buttons.js";

class AliasRowComponent {
  constructor({ parent }, data) {
    this.data = data;
    this.parent = parent;

    this.fromEl = el("input.alias-from", {
      value: this.data.source || "",
      placeholder: "From",
    });
    this.fromEl.addEventListener("input", () => {
      this.data.source = this.fromEl.value;
      this.valuesUpdated();
    });

    this.toEl = el("input.alias-to", {
      value: this.data.target || "",
      placeholder: "To",
    });
    this.toEl.addEventListener("input", () => {
      this.data.target = this.toEl.value;
      this.valuesUpdated();
    });

    this.delButtonEl = el("button.icon-button", { type: "button" }, [
      graphButton(
        "trash",
        "M3 0c-.55 0-1 .45-1 1h-1c-.55 0-1 .45-1 1h7c0-.55-.45-1-1-1h-1c0-.55-.45-1-1-1h-1zm-2 3v4.81c0 .11.08.19.19.19h4.63c.11 0 .19-.08.19-.19v-4.81h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1z",
        {}
      ),
    ]);
    this.delButtonEl.addEventListener("click", () => this.delete());

    this.el = el("div.alias-row", [
      this.fromEl,
      el("span", "→"),
      this.toEl,
      this.delButtonEl,
    ]);
  }

  update(data) {
    this.data = data;
    this.fromEl.value = data.source || "";
    this.toEl.value = data.target || "";
  }

  valuesUpdated() {
    this.parent.updateInternal();
  }

  delete() {
    this.parent.deleteChild(this.data);
  }
}

export { AliasRowComponent };
