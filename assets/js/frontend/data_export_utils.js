import "file-saver";
import { mount, setChildren } from "redom";

import LoadingIndicatorComponent from "../common/loading-indicator.component.js";

const XP_EXPORT_PATH = "/my/pulses";
const PRIVATE_EXPORT_PATH = "/my/private";

/**
 * Initialise data export functionality rendered on page.
 */
function initExport() {
  const container = document.getElementById("export-data-container");
  const button = document.getElementById("export-data-button");
  const indicator_el = document.getElementById("export-data-processing");
  const include_private_el = document.getElementById("include-private-data");
  const since_el = document.getElementById("export-since");

  if (button != null) {
    button.onclick = async () => {
      container.hidden = true;
      mount(indicator_el, new LoadingIndicatorComponent());

      const include_private = include_private_el.checked;
      const since = since_el ? since_el.value : null;

      const xp_export_url = new URL(XP_EXPORT_PATH, window.location.href);
      if (since) {
        xp_export_url.searchParams.append("since", since);
      }

      const promises = [
        fetch(xp_export_url, {
          method: "GET",
          headers: {
            accept: "text/csv",
          },
          credentials: "same-origin",
        }),
      ];

      if (include_private) {
        promises.push(
          fetch(PRIVATE_EXPORT_PATH, {
            method: "GET",
            headers: {
              accept: "text/csv",
            },
            credentials: "same-origin",
          })
        );
      }

      try {
        const resp = await Promise.all(promises);

        const blob = await resp[0].blob();
        globalThis.saveAs(blob, "pulses.csv", { autoBom: false });

        if (resp.length === 2) {
          // Wait for a moment before continuing, to fix downloading multiple files on Chrome
          // See https://github.com/eligrey/FileSaver.js/issues/435

          setTimeout(async () => {
            const blob = await resp[1].blob();
            globalThis.saveAs(blob, "private.csv", {
              autoBom: false,
            });
          }, 500);
        }
      } catch (err) {
        alert("Error exporting data:\n\n" + err.message);
        console.error(err);
      }

      container.hidden = false;
      setChildren(indicator_el, []);
    };
  }
}

export { initExport };
