defmodule CodeStatsWeb.ProfileLiveTest do
  use CodeStatsWeb.ConnCase

  alias CodeStats.User.Machine
  alias CodeStats.User.Pulse
  alias CodeStats.XP
  alias CodeStats.Language
  alias CodeStats.UserHelpers

  test "viewing a nonexistent user should not crash", %{conn: conn} do
    conn = get(conn, "/users/foobar")

    assert conn.status == 200
    assert conn.resp_body =~ "HTTP/2 404"

    {:ok, _view, _html} = live(conn)
  end

  test "viewing a profile with a language hidden in recent pulse should not crash", %{conn: conn} do
    {:ok, language1} = Language.get_or_create("elixir")
    {:ok, language2} = Language.get_or_create("php")

    {:ok, user} = UserHelpers.create_user("user@somewhere", "test_user")

    create_pulse_with_some_hidden_xps(user, language1, language2)

    conn = get(conn, "/users/test_user")

    assert conn.status == 200

    {:ok, _view, _html} = live(conn)
  end

  defp create_pulse_with_some_hidden_xps(user, visible_language, hidden_language) do
    {:ok, machine} =
      Machine.create_changeset(%{name: "test_machine", user: user})
      |> Repo.insert()

    sent_at = DateTime.utc_now() |> DateTime.truncate(:second)
    sent_at_local = DateTime.to_naive(sent_at)

    {:ok, pulse} =
      Pulse.changeset(%Pulse{sent_at: sent_at, tz_offset: 0, sent_at_local: sent_at_local}, %{})
      |> Ecto.Changeset.put_change(:user_id, user.id)
      |> Ecto.Changeset.put_change(:machine_id, machine.id)
      |> Repo.insert()

    base_xp =
      XP.changeset(%XP{amount: 1})
      |> Ecto.Changeset.put_change(:pulse_id, pulse.id)

    {:ok, _} =
      base_xp
      |> Ecto.Changeset.put_change(:language_id, visible_language.id)
      |> Ecto.Changeset.put_change(:original_language_id, visible_language.id)
      |> Repo.insert()

    {:ok, _} =
      base_xp
      |> Ecto.Changeset.put_change(:language_id, nil)
      |> Ecto.Changeset.put_change(:original_language_id, hidden_language.id)
      |> Repo.insert()
  end
end
