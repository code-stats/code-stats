defmodule CodeStats.XP do
  import CodeStats.Utils.TypedSchema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2, where: 3]

  deftypedschema "xps" do
    field(:amount, :integer, integer())
    belongs_to(:pulse, CodeStats.User.Pulse, CodeStats.User.Pulse.t())
    belongs_to(:language, CodeStats.Language, CodeStats.Language.t() | nil)

    # Original language can be used to fix alias errors later, it should always use
    # the language that was sent. :language field on the other hand follows aliases
    belongs_to(:original_language, CodeStats.Language, CodeStats.Language.t())
  end

  @doc """
  Creates a changeset based on the `data` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(data, params \\ %{}) do
    data
    |> cast(params, [:amount])
    |> validate_required([:amount])
  end

  @doc """
  Get all of a user's XP's by user ID.

  Optionally filter by the `sent_at_local` timestamp to only get XP newer than a given date.
  """
  @spec xps_by_user_id(integer, nil | Date.t()) :: Ecto.Query.t()
  def xps_by_user_id(user_id, since \\ nil) when is_integer(user_id) do
    q =
      from(
        x in __MODULE__,
        join: p in CodeStats.User.Pulse,
        on: p.id == x.pulse_id,
        where: p.user_id == ^user_id,
        preload: [:language, pulse: {p, :machine}]
      )

    case since do
      nil ->
        q

      %Date{} = dt ->
        naive_dt = NaiveDateTime.new!(dt, ~T[00:00:00])
        q |> where([_x, p], p.sent_at_local >= ^naive_dt)
    end
  end
end
