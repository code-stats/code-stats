defmodule CodeStats.Profile.SchemaObjects do
  use Absinthe.Schema.Notation

  alias CodeStats.Profile.Queries
  alias CodeStats.Profile.Queries.Flow, as: FlowQ

  @desc "User profile public data"
  object :profile do
    @desc "Timestamp when user registered into service"
    field(:registered, :datetime)

    @desc "Total amount of XP of user"
    field :total_xp, :integer do
      resolve(fn %{cache: cache}, _, _ ->
        {:ok, Queries.cached_total(cache)}
      end)
    end

    @desc "User's languages and their XP"
    field :languages, list_of(:profile_language) do
      arg(:since, type: :datetime)
      arg(:no_cache, type: :boolean)

      resolve(fn
        %{id: uid}, %{since: since}, _ ->
          {:ok, Queries.language_xps(uid, since)}

        %{id: uid}, %{no_cache: true}, _ ->
          {:ok, Queries.aliased_languages(uid)}

        %{cache: cache}, _, _ ->
          {:ok, Queries.cached_languages(cache)}
      end)
    end

    @desc "User's languages before aliasing and their XP"
    field :unaliased_languages, list_of(:profile_language) do
      resolve(fn %{id: uid}, _, _ -> {:ok, Queries.unaliased_languages(uid)} end)
    end

    @desc "User's machines and their XP"
    field :machines, list_of(:profile_machine) do
      arg(:since, type: :datetime)

      resolve(fn
        %{id: uid}, %{since: since}, _ ->
          {:ok, Queries.machine_xps(uid, since)}

        %{cache: cache}, _, _ ->
          {:ok, Queries.cached_machines(cache)}
      end)
    end

    @desc "User's dates when they have been active and their XP"
    field :dates, list_of(:profile_date) do
      arg(:since, type: :date)

      resolve(fn
        %{cache: cache}, %{since: since}, _ ->
          {:ok, Queries.cached_dates(cache, since)}

        %{cache: cache}, _, _ ->
          {:ok, Queries.cached_dates(cache)}
      end)
    end

    @desc "User's dates since given date with their summed XP per language per day"
    field :day_language_xps, list_of(:profile_daylanguage) do
      arg(:since, type: non_null(:date))

      resolve(fn %{id: uid}, %{since: since}, _ ->
        {:ok, Queries.day_languages(uid, since)}
      end)
    end

    @desc "User's XP by day of week"
    field :day_of_week_xps, list_of(:profile_dow_xp) do
      arg(:since, type: :date)

      resolve(fn
        %{cache: cache}, %{since: since}, _ ->
          dow_data =
            Queries.cached_days_of_week(cache, since)
            |> Map.to_list()
            |> Enum.map(fn {day, xp} -> %{day: day, xp: xp} end)

          {:ok, dow_data}

        %{cache: cache}, _, _ ->
          dow_data =
            Queries.cached_days_of_week(cache)
            |> Map.to_list()
            |> Enum.map(fn {day, xp} -> %{day: day, xp: xp} end)

          {:ok, dow_data}
      end)
    end

    @desc "User's XP by day of year"
    field :day_of_year_xps, :profile_day_of_year_xp do
      resolve(fn %{cache: cache}, _, _ ->
        date_to_key = fn date ->
          Date.day_of_year(date)
        end

        # 2000 was a leap year, so it contains all possible days (includes leap day)
        doys =
          Date.range(~D[2000-01-01], ~D[2000-12-31])
          |> Enum.reduce(%{}, fn date, acc ->
            Map.put(acc, date_to_key.(date), 0)
          end)

        doy_data =
          Queries.cached_dates(cache)
          |> Enum.reduce(doys, fn %{date: date, xp: xp}, acc ->
            # Also move the XP date to 2000 so that day of year number matches correctly
            date = Date.from_iso8601!(date)
            {:ok, moved_date} = Date.new(2000, date.month, date.day)
            key = date_to_key.(moved_date)
            Map.update!(acc, key, &(&1 + xp))
          end)

        {:ok, doy_data}
      end)
    end

    @desc "User's XP by hour of day (in 24 hour format)"
    field :hour_of_day_xps, :profile_hour_of_day_xp do
      resolve(fn %{cache: cache}, _, _ ->
        hod_data = Queries.cached_hours(cache)
        {:ok, hod_data}
      end)
    end

    @desc "User's top notable flow states"
    field :top_flows, :flow_meta_info do
      resolve(fn %{cache: cache}, _, _ ->
        FlowQ.meta(cache)
      end)
    end

    @desc "User's top languages in flow states"
    field :top_flow_languages, :flow_top_languages do
      resolve(fn %{cache: cache}, _, _ ->
        FlowQ.top_languages(cache)
      end)
    end

    @desc "User's minutes spent in flow state by day"
    field :flow_mins_by_day, list_of(:flow_date) do
      arg(:since, type: :date)

      resolve(fn %{cache: cache}, %{since: since}, _ ->
        FlowQ.flow_mins_per_day(cache, since)
      end)
    end
  end

  @desc "Language and its total XP for a profile"
  object :profile_language do
    field(:name, :string)
    field(:xp, :integer)
  end

  @desc "Machine and its total XP for a profile"
  object :profile_machine do
    field(:name, :string)
    field(:xp, :integer)
  end

  @desc "Date when user was active and its total XP for a profile"
  object :profile_date do
    # Comes straight as ISO date format from cache
    field(:date, :string)
    field(:xp, :integer)
  end

  @desc "Date when profile has an amount of XP of the language"
  object :profile_daylanguage do
    field(:date, :date)
    field(:language, :string)
    field(:xp, :integer)
  end

  @desc "Day of week and its combined XP"
  object :profile_dow_xp do
    field(:day, :integer)
    field(:xp, :integer)
  end

  @desc "Map of the top notable flow states and other metadata"
  object :flow_meta_info do
    field(:longest, :flow)
    field(:most_prolific, :flow)
    field(:strongest, :flow)
    field(:average_xp, :float)
    field(:average_duration, :float)
  end

  @desc "A singular flow state with local time"
  object :flow do
    field(:start_time_local, :naive_datetime)
    field(:duration, :integer)
    field(:xp, :integer)
    field(:languages, :flow_languages)
  end

  @desc "A date and its amount of minutes spent in flow state"
  object :flow_date do
    field(:date, :date)
    field(:mins, :integer)
  end

  scalar :flow_languages, description: "Map of languages and their XP in flow" do
    serialize(& &1)
  end

  scalar :profile_day_of_year_xp,
    description:
      "Map where key is number of day of year (including leap day, like in the year 2000) and value is amount of XP" do
    serialize(& &1)
  end

  scalar :profile_hour_of_day_xp,
    description: "Map where key is hour of day (in 24 hour format) and value is amount of XP" do
    serialize(& &1)
  end

  scalar :flow_top_languages,
    description:
      "Map where key is language name and value contains various stats about flows in given language" do
    serialize(fn data ->
      for {k, v} <- data, into: %{} do
        {k,
         %{
           totalXp: v.total_xp,
           totalDuration: v.total_duration,
           averageXp: v.average_xp,
           averageDuration: v.average_duration,
           amount: v.amount
         }}
      end
    end)
  end

  scalar :datetime, description: "RFC3339 time with UTC offset" do
    serialize(&CodeStats.DateUtils.to_rfc3339/1)

    parse(fn %Absinthe.Blueprint.Input.String{value: dt} ->
      # rfc3339_utc might crash with missing time offset, see bug: https://github.com/lau/calendar/issues/50
      case DateTime.from_iso8601(dt) do
        {:ok, val, _} -> {:ok, val}
        _ -> :error
      end
    end)
  end

  scalar :naive_datetime, description: "ISO 8601 naive datetime" do
    serialize(&NaiveDateTime.to_iso8601/1)
  end

  scalar :date, description: "ISO 8601 date" do
    serialize(&Date.to_iso8601(&1))

    parse(fn %Absinthe.Blueprint.Input.String{value: dt} ->
      case Date.from_iso8601(dt) do
        {:ok, val} -> {:ok, val}
        _ -> :error
      end
    end)
  end
end
