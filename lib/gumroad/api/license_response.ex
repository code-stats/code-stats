defmodule Gumroad.Api.LicenseResponse do
  import CodeStats.Utils.TypedStruct

  deftypedstruct(%{
    success: boolean(),
    uses: non_neg_integer(),
    purchase: Gumroad.Api.PurchaseData.t()
  })

  @doc """
  Deserialize a decoded license response.
  """
  @spec deserialize(map()) :: t()
  def deserialize(map) do
    %__MODULE__{
      success: map["success"],
      uses: map["uses"],
      purchase: Gumroad.Api.PurchaseData.deserialize(map["purchase"])
    }
  end
end
