defmodule Gumroad.Api.PurchaseData do
  import CodeStats.Utils.TypedStruct
  alias Gumroad.Api.DeserializeError

  deftypedstruct(%{
    product_id: String.t(),
    email: String.t(),
    order_number: integer(),
    sale_id: String.t(),
    sale_timestamp: DateTime.t(),
    subscription_id: String.t(),
    variants: String.t(),
    license_key: String.t(),
    refunded: boolean(),
    disputed: boolean(),
    dispute_won: boolean(),
    subscription_ended_at: DateTime.t() | nil,
    subscription_cancelled_at: DateTime.t() | nil,
    subscription_failed_at: DateTime.t() | nil
  })

  @doc """
  Deserialize the purchase data part of a decoded license response.
  """
  @spec deserialize(map()) :: t()
  def deserialize(map) do
    with {:ok, sale_timestamp, _} <-
           map |> Map.fetch!("sale_timestamp") |> DateTime.from_iso8601(),
         subscription_ended_at = from_datetime(map, "subscription_ended_at"),
         subscription_cancelled_at = from_datetime(map, "subscription_cancelled_at"),
         subscription_failed_at = from_datetime(map, "subscription_failed_at") do
      %__MODULE__{
        product_id: map["product_id"],
        email: map["email"],
        order_number: map["order_number"],
        sale_id: map["sale_id"],
        sale_timestamp: sale_timestamp,
        subscription_id: map["subscription_id"],
        variants: map["variants"],
        license_key: map["license_key"],
        refunded: map["refunded"],
        disputed: map["disputed"],
        dispute_won: map["dispute_won"],
        subscription_ended_at: subscription_ended_at,
        subscription_cancelled_at: subscription_cancelled_at,
        subscription_failed_at: subscription_failed_at
      }
    else
      err -> raise DeserializeError, "Invalid response data: #{inspect(err)}"
    end
  end

  @spec from_datetime(map(), String.t()) :: DateTime.t() | nil
  defp from_datetime(map, key) do
    val = Map.fetch!(map, key)

    if val != nil do
      with {:ok, dt, _} <- DateTime.from_iso8601(val) do
        dt
      else
        _ -> raise DeserializeError, "Invalid datetime '#{inspect(val)}' from API"
      end
    else
      nil
    end
  end
end
