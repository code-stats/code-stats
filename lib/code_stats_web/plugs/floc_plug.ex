defmodule CodeStatsWeb.FLoCPlug do
  @moduledoc """
  Plug to opt out of Chrome's Federated Learning of Cohorts.
  """

  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  @spec call(Plug.Conn.t(), any) :: Plug.Conn.t()
  def call(conn, _opts) do
    Plug.Conn.put_resp_header(conn, "permissions-policy", "interest-cohort=()")
  end
end
