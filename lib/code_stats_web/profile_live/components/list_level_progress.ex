defmodule CodeStatsWeb.ProfileLive.Components.ListLevelProgress do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.Components

  @moduledoc """
  Level progress in a single target (language, machine…), list item version.

  Requires:
    @target
    @total_xp
    @new_xp
    @show_bar (true to show progress bar, false to not)
    @class (classes for li as string)
    @id
  """

  @impl true
  def render(assigns) do
    ~H"""
    <li class={"level-counter #{@class}"}>
      <strong class="level-prefix">
        <%= @target %>
      </strong>
      level
      <.live_component
        module={Components.LevelText}
        id={"#{@id}-level-text"}
        total_xp={@total_xp}
        new_xp={@new_xp}
      />

      <%= if @show_bar do %>
        <.live_component
          module={Components.ProgressBar}
          id={"#{@id}-progress-bar"}
          total_xp={@total_xp}
          new_xp={@new_xp}
          widths={Components.LevelProgress.get_xp_bar_widths(@total_xp, @new_xp)}
        />
      <% end %>
    </li>
    """
  end
end
