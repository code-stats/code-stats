defmodule CodeStatsWeb.ProfileLive do
  use CodeStatsWeb, :live_view

  require Logger

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.Profile.PermissionUtils
  alias CodeStatsWeb.AuthUtils

  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.DataProviders

  @impl Phoenix.LiveView
  def mount(%{"username" => username}, session, socket) do
    socket = assign(socket, connected?: connected?(socket), current_user: nil)

    with {:ok, user} <- get_user(username),
         authed_user_id <- AuthUtils.get_current_user_id_from_session(session),
         authed_user <- authed_user_id && AuthUtils.get_user_by_id(authed_user_id),
         true <-
           PermissionUtils.can_access_profile?(authed_user, user) do
      if connected?(socket) do
        Pulse.PubSub.subscribe(user.id)
        User.Cache.PubSub.subscribe(user.id)
      end

      totals_graph = Graphs.Catalogue.totals_graph()
      other_graphs = Graphs.Catalogue.other_graphs()
      data_providers = Graphs.Catalogue.data_providers([totals_graph | other_graphs])
      required_shared_data = DataProviders.Utils.required_shared_data(data_providers)
      shared_data = DataProviders.SharedData.get_data(required_shared_data, user)
      initial_data = Graphs.Catalogue.get_initial_data(shared_data, data_providers, user)
      socket = Graphs.Catalogue.assign_datas(socket, data_providers, initial_data)

      paid_user = CodeStats.User.Paid.UserUtils.paid_user_for_user(authed_user)
      paid_viewed_user = CodeStats.User.Paid.UserUtils.paid_user_for_user(user)

      socket =
        if connected?(socket) do
          show_ads? = CodeStatsWeb.AdPlug.show_ad?(paid_user, paid_viewed_user)

          assign(socket, show_ads?: show_ads?)
        else
          assign(socket, show_ads?: false)
        end

      {:ok,
       assign(socket,
         user: user,
         current_user: authed_user,
         totals_graph: totals_graph,
         other_graphs: other_graphs,
         title: username
       )}
    else
      _ -> {:ok, assign(socket, user: nil)}
    end
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({Pulse.PubSub, :pulse, %Pulse.PubSub.Data{} = data}, socket) do
    graphs = [Graphs.Catalogue.totals_graph() | Graphs.Catalogue.other_graphs()]
    data_providers = Graphs.Catalogue.data_providers(graphs)
    old_datas = Graphs.Catalogue.get_assigned_datas(socket, data_providers)

    updated_datas =
      DataProviders.Utils.update_providers(
        data_providers,
        old_datas,
        socket.assigns.user,
        data.pulse,
        data.cache
      )

    socket = Graphs.Catalogue.assign_datas(socket, data_providers, updated_datas)

    {:noreply, socket}
  end

  def handle_info({User.Cache.PubSub, :cache, _cache}, socket) do
    # If user's cache is updated, refresh the page as our live updates could be wrong otherwise
    # (e.g. language has new aliased name, machine has new name…)
    {:noreply,
     push_navigate(socket,
       to: ~p"/users/#{socket.assigns.user.username}",
       replace: true
     )}
  end

  def handle_info(msg, socket) do
    Logger.warning(inspect(msg), label: "Got stray msg:")
    {:noreply, socket}
  end

  # Fix the username specified in the URL by converting plus characters to spaces.
  # This is not done by Phoenix for some reason.
  defp fix_url_username(username) do
    String.replace(username, "+", " ")
  end

  defp get_user(username) do
    with username <- fix_url_username(username),
         %User{} = user <- User.get_by_username(username, true) do
      {:ok, user}
    else
      _ -> :error
    end
  end
end
