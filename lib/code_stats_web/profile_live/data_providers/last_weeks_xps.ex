defmodule CodeStatsWeb.ProfileLive.DataProviders.LastWeeksXPs do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.XP
  alias CodeStats.Profile.Queries

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  @max_languages 10

  defmodule Data do
    deftypedstruct(%{
      dataset: [{String.t(), [%{date: Date.t(), xp: integer()}]}],
      raw_data: any()
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(CodeStatsWeb.ProfileLive.DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(_shared_data, user) do
    now = Date.utc_today()
    then = Date.add(now, -CodeStatsWeb.ProfileUtils.last_days_amount() + 1)

    data = Queries.day_languages(user.id, then)

    dataset = generate_dataset(data, then)

    %Data{dataset: dataset, raw_data: data}
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, pulse, _cache) do
    new_items =
      for %XP{} = xp <- pulse.xps do
        %{
          language: xp.language.name,
          date: NaiveDateTime.to_date(pulse.sent_at_local),
          xp: xp.amount
        }
      end

    new_data = data.raw_data ++ new_items

    now = Date.utc_today()
    then = Date.add(now, -CodeStatsWeb.ProfileUtils.last_days_amount() + 1)

    dataset = generate_dataset(new_data, then)

    %Data{
      data
      | raw_data: new_data,
        dataset: dataset
    }
  end

  defp generate_dataset(data, start_date) do
    language_groups = Enum.group_by(data, & &1.language)

    unsorted_dataset =
      for {language, language_data} <- language_groups do
        by_date = Enum.group_by(language_data, & &1.date)
        total_xp = Enum.reduce(language_data, 0, &(&1.xp + &2))

        filtered_dates =
          Enum.filter(by_date, fn {date, _} ->
            Date.compare(date, start_date) in [:eq, :gt]
          end)

        {{language, total_xp},
         Enum.map(filtered_dates, fn {date, xps} ->
           %{date: date, xp: Enum.reduce(xps, 0, &(&1.xp + &2))}
         end)}
      end

    sorted_dataset =
      unsorted_dataset
      |> Enum.sort_by(fn {{_, total}, _} -> total end, :desc)
      |> Enum.map(fn {{lang, _}, data} -> {lang, data} end)

    {top_langs, other_langs} = Enum.split(sorted_dataset, @max_languages)

    if other_langs != [] do
      grouped_other_langs =
        other_langs
        |> Enum.map(&elem(&1, 1))
        |> List.flatten()
        |> Enum.group_by(& &1.date)
        |> Enum.reduce([], fn {date, data}, acc ->
          sum = data |> Enum.map(& &1.xp) |> Enum.sum()
          [%{date: date, xp: sum} | acc]
        end)

      top_langs ++ [{"Others", grouped_other_langs}]
    else
      top_langs
    end
  end
end
