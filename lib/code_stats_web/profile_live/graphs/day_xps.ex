defmodule CodeStatsWeb.ProfileLive.Graphs.DayXPs do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  # Year 2000 was a leap year
  day_list =
    Enum.reduce(1..366, %{}, fn doy, acc ->
      date = CodeStats.DateUtils.date_from_day_of_leap_year(doy)
      Map.update(acc, date.month, [doy], &(&1 ++ [doy]))
    end)
    |> Enum.sort_by(&elem(&1, 0))

  @day_list day_list

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.DayXPs])

  @impl true
  def update(assigns, socket) do
    max_xp = assigns.days |> Map.values() |> Enum.max(fn -> 0 end)
    new_assigns = Map.put(assigns, :max, max_xp)
    {:ok, assign(socket, new_assigns)}
  end

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if assigns[DataProviders.DayXPs].days != %{} do %>
      <.live_component
        module={__MODULE__}
        id={:day_xps_graph}
        days={assigns[DataProviders.DayXPs].days}
        day_list={day_list()}
      />
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end

  @spec day_list() :: [{1..12, [1..366]}]
  defp day_list(), do: @day_list
end
